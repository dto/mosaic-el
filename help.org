#+TITLE: SchemeMosaic general help

Welcome to SchemeMosaic! This help screen answers some basic questions
for new users, especially those who are new to GNU Emacs.

You can click the stars "**" at the left of each question to show the
answer, or move the cursor to that line and press the tab key. Use the
mouse wheel, arrow keys, or PageUp/PageDown keys to scroll up and
down.

To show all the answers at once, choose the "Org" menu above, and
select the submenu "Show/Hide", then click "Show All." 

Beyond this screen is also the Emacs help system. Check out the "Help"
menu above to find out more.

** How do I use Emacs?

Most things can be done with the mouse, but you must learn several
keys. 

  - Alt-X is how you start entering a command into the text area at
    the bottom. (For historical reasons this is going to be referred
    to as "M-x" in other documentation.)
  - Control-G is the typical way to cancel a command, stop a
    subprocess that isn't responding, quit out of any undesirable
    questions that come up at the bottom, and so on. If this doesn't
    work, try hitting ESCAPE three times.
  - The thing at the bottom I keep mentioning is called the "Minibuffer".
  - The "TAB" key does smart auto-completion of stuff in the Minibuffer.
  - All the other text areas you see are called regular plain old
    "buffers". Buffers can display/edit a file on disk, a directory,
    an image, a webpage, an IRC channel, or display various controls
    and task buttons along with data. Check the "Buffers" menu for
    ways to browse and switch between buffers. There's even a
    clickable buffer-list buffer! This help screen is just another
    buffer, too. 

Another thing to remember is that undo/redo and cut-and-paste
functionality use different keys:

  - Control-W (Cut)
  - Control-Y (Paste)
  - Alt-Y (Copy)
  - Control-SPACE (Begin marking a region)
  - Escape (Clear mark)
  - Control-/  (Undo/Redo)
  - Control-Shift-/   (Redo, in cell sheets)

You can use the "Edit" menu to cut/paste/copy until you remember the
keys. The key bindings are helpfully indicated on the menu
itself. (These menu items do not currently work in the Sequencer, but
the keybindings do.)

Emacs is super customizable so you can change basically anything you
want. Check out "Options/Customize Emacs" in the menu.

** What is the license for EmacsMosaic/SchemeMosaic? 

EmacsMosaic and SchemeMosaic are Free Software released under the
terms of the GNU General Public License, Version 3. You can find a
copy included in the Mosaic project directory, in a file called
LICENSE. You can also find the full text at:

 - https://www.gnu.org/licenses/gpl-3.0.html

Several Emacs Lisp library components are also included. These are:

  - midi.el, jack.el, and ecasound.el (by Mario Lang, GPL)
  - inf-snd.el (by Michel Scholz, MIT License)
  - cell.el (by David O'Toole, GPL)

See each file for full copyright and licensing information.

Audio functionality is built on Bill Schottstaedt's Snd editor and the
Ecasound HDR application. The Mosaic project installation includes
full licensing information and sources for the included Emacs Lisp
components; on platforms where Ecasound and Snd are shipped alongside
Mosaic, full sources are included for the applications themselves as
well. Please see their respective websites for more information.

 - http://nosignal.fi/ecasound/
 - https://ccrma.stanford.edu/software/snd/snd/snd.html 

On Windows systems, Mosaic works with Cygwin and a number of its
packages. See http://cygwin.org/ for full information on Cygwin.

The `defun-memo' facility is based on code by Peter Norvig
for his book "Paradigms of Artificial Intelligence
Programming". The modified version is redistributed here.

You can find more information on Norvig's book, and the full license
for the `defun-memo' code, at his website:

 - http://www.norvig.com/paip.html
 - http://www.norvig.com/license.html

The neural network implementation is in the public domain, by Scott
E. Fahlman for Carnegie-Mellon University.

** What are the current known issues?

 - Spreadsheet cursor displays incorrectly in image cells and with
   some Unicode characters.
 - Error message and corrupted display when mouse-drag begins or ends
   outside of spreadsheet display area. Reinitialize cell-mode by
   issuing the command "M-x cell-mode RET" to restore your display if
   this happens.
 - Pitch-shifting and time-stretching implementations need fine tuning.
 - Musical symbol keybindings, MIDI support, and PulseAudio support
   are not working on MS Windows.
 - Emacspeak support and Freesound.org integration are partial and
   experimental.

** How do I see a list of all available commands?

Hit Alt-X, then type "mosaic" and press TAB twice to get a clickable
list of completions.

** How do I enable Emacspeak support?
   
Currently there is basic Emacspeak support for the Mosaic spreadsheet
user interface. Use the command "mosaic-enable-emacspeak" to enable
it. That is, press Alt-X, then type "mosaic-enable-emacspeak" then
press RETURN.

After this loads, you can use "C-c s" to speak the current cell value,
or "C-c p" to speak the current position. It will also speak the
current cell value whenever you move the cursor into a new cell, and
notify you when you have tried to move beyond the edge of the
spreadsheet. More functionality will be included soon.

Emacspeak must already be installed and running. Installing and
configuring Emacspeak is outside the scope of this help file, but you
can find more information at

http://tvraman.github.io/emacspeak/manual/

** How can I see what sound device has what ALSA device number?

Use the command M-x "mosaic-show-sound-cards".

** How do I change the font?

Select "Options/Set Default Font" from the menu. Don't forget to
choose "Save Options" after making the changes you want.

** How do I change settings specific to EmacsMosaic/SchemeMosaic? 

See the menu item "Mosaic/Customize global settings"

** Where are my settings saved? 

Your customization settings are saved in a special text file called
".emacs" in your home directory. (Windows users will find this in
"C:\Cygwin64\home\MYUSERNAME".) 

You can use "File/Open file" from the menu to edit this file and even
add your own startup commands. That works because Emacs reads this
file at startup. 

** How do I split subwindows, close them, and resize them?

  - Close window: Right click the subwindow's "Modeline", the colored
    horizontal info bar at the bottom of the subwindow.
  - Split window: Choose "File/New window below" or "File/New window
    at right" from the menu.
  - Resize window: Drag the modeline upward/downward, or drag the
    vertical divider to the left or right.

** How can I switch between subwindows without using the mouse?

  * Use Control-TAB to switch to the next subwindow.
  * Use Shift-ARROW to switch to the subwindow in the direction of the
    arrow key chosen.

** My window configuration changed. How do I get it back?

Try entering the command "winner-undo". That is, hit Alt-X, then type
"winner-undo", then hit RETURN. More than one invocation may be
required. Winner-undo can also be invoked with Control-X LeftArrow.

** How do I make multiple Emacs windows on my desktop?

See "File/New Frame".

** What are the basic concepts of Mosaic? 

A "session" organizes all the audio and data files for a given song or
project into one directory. A session can have many audio files, and
one or more special files called "sheets". These have the file
extension ".cell" and include the various screens for doing things in
Mosaic.

A "Sequencer" sheet contains musical notes; you can have many
sequencer sheets in one session. "Chopper" sheets are for chopping and
screwing beats and concatenative synthesis exploration. "Looper"
sheets are for interactively listening to / playing with multitrack
loops, backing tracks, a cappella vocals, etc and recording the result
to disk.

When a sheet is selected and you choose the "Render current sheet"
option, Mosaic attempts to render the given sheet either to a WAV file
(when can then be auditioned in the Looper), or to a series of WAV
files that are fed to the Looper for further experimentation.

** How do I start Mosaic? 

Choose the menu item "Start EmacsMosaic/SchemeMosaic" under the
"Mosaic" menu.

** What is the current status of the Mosaic project?

All main areas of functionality (chopping, screwing, looping,
sequencing, and more) have been implemented and are working; however,
the user interface is heavily under construction and some features are
not yet available from the GUI. 

** Where is the documentation? 

See http://xelf.me/scheme-mosaic.html and
http://xelf.me/emacs-mosaic.html. Unfortunately the documentation is
currently incomplete.

** How do I see the list of keybindings available in the Sequencer?

Use the menu: choose "Help", then "Describe...", then "Describe buffer
modes." This works in any buffer actually, and will give tell you
which keys go with which commands in that buffer.

You can also use the "Sheet" menu (next to the "Mosaic" menu in the
menubar). When a sheet is selected and you open this menu, the current
keybindings will be displayed alongside the menu items.

** How do I open one of the example sessions? 

Go to the "Mosaic" menu, choose the "Session" submenu, and select
"Load (or create) a session". Then navigate to the "sessions" folder,
and choose either "test.mosaic" or "violin.mosaic".

(The examples are included on Windows, but may not be on other
platforms. See http://xelf.me/guide.html for more information.)

To mess around with the chop-and-screw functionality, check out
test.mosaic's file "chopper.cell" and try substituting your own files
or changing the values in the spreadsheet. 

** How do I save all unsaved buffers, without having to save them one at a time?

Press Control-X, then press the letter "s". This triggers the command
"save-some-buffers". You could also hit Alt-X and then type
"save-some-buffers" before hitting RETURN, or use the menu option
"File/Save some buffers...".

As a convenience, the menu option "Mosaic/Session/Save session"
invokes the "save-some-buffers" command for you, in addition to saving
any data specific to the current Mosaic session.

** Why are my sheet's columns misaligned and/or colors are missing?

Reset the sheet by using the command M-x "cell-mode". This issue will
be fixed in a future version.

** What if I would prefer a lighter (or otherwise different) color theme?

Check out "Options/Customize Emacs/Custom Themes". Because some
settings are specific to Mosaic, the other themes might still not
quite look right when Mosaic buffers are displayed. You can either
customize the Mosaic faces yourself (using M-x customize-group,
"cell") or try the "mosaic-light" theme by issuing the following
command:

  M-x load-file RETURN mosaic-theme.el

This theme is not presently 100% complete but will include the missing
colors soon.

** How do I report problems or ask for help?

You can email David O'Toole at deeteeoh1138@gmail.com (or
dto@xelf.me).

If a command causes an error, try turning on "Enter debugger on error"
in the "Options" menu, and then run the command again. A backtrace
should be produced, which can be shared with the developer(s) to help
diagnose and fix the issue. You can also use the command M-x
"toggle-debug-on-error".

** How can I get interactive help from real humans?

You can chat live with other Emacs users, and even ask for
Mosaic-specific help, via IRC (Internet Relay Chat).

For a web-based interface to IRC, check out:

  https://webchat.freenode.net/

Or try connecting to IRC right from within Emacs! With the command
"M-x erc". That is, press Alt-X, then type "erc" and hit RETURN.

Please be sure to ask your question on the proper channel! For general
Emacs help you can visit the "#emacs" channel; whereas for help
specific to Mosaic, visit the channel "#xelf" and ask for user "dto"
(David O'Toole). I will try to answer questions to the best of my
ability during the day (in U.S. Eastern Standard Time.)

** How can I set up Cygwin for running Mosaic by remote?

 - Install cygwin packages "openssh-server" and "xauth"
 - Open Cygwin shell as administrator 
 - Run ssh-host-config program
 - Edit /etc/sshd_config to include "X11Forwarding yes"
 - cygrunsrv --start cygsshd
 - from the other machine: ssh -Y Username@IP

