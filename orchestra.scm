;;; orchestra.scm                         -*- lexical-binding: t; -*-

;; Copyright (C) 2020-2021  David O'Toole

;; Author: David O'Toole <dto@xelf.me> <deeteeoh1138@gmail.com>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Liquid orchestra.

(define *orch-index* 0)
(define *orch-polyoid* #f)
(define *orch-polyoid-params* #f)
(define *orch-amps* #f)
(define *orch-phases* #f)
(define *orch-vibrato-env* #f)
(define *orch-amp-env* #f)
(define *orch-vibrato-wave* #f)
(define *orch-random-vibrato-wave* #f)
(define *orch-gls-env* #f)

(define (partials->polyoid-params partials)
  (if (pair? partials)
      (append (list (partials 0)
                    (partials 1)
                    0.0)
              (partials->polyoid-params (cdr (cdr partials))))
      ()))

(define (partials->amps partials duration)
  (if (pair? partials)
      (cons (make-env '(0 1 1 1)
                      ;; grab existing amp
                      (car (cdr partials))
                      duration)
            (partials->amps (cdr (cdr partials))
                            duration))))

(define (partials->phases partials duration)
  (if (pair? partials)
      (cons (make-env '(0 1 1 1)
                      1.0
                      duration)
            (partials->phases (cdr (cdr partials))
                              duration))))

;; this is modified from generators.scm in Snd sources
(define (polyoid-env^ gen fm amps phases original-data attack-noise-gen noise-amp)
  ;; (let ((original-data (polyoid-partial-amps-and-phases gen)))
  (let ((data-len (length original-data))
        (amps-len (length amps))
        (tn (polyoid-tn gen))
        (un (polyoid-un gen)))
    (do ((i 0 (+ i 3))
         (j 0 (+ j 1)))
        ((or (= j amps-len)
             (= i data-len)))
      (let ((hn (floor (original-data i)))
            (amp (+ (env (amps j))
                    ;; don't add noise to first few partials
                    (if (< j 4)
                        0.0
                        (* (/ 1.8 (+ 1 j)) ;; attenuate noise as we go up the list of partials
                           noise-amp
                           (rand-interp attack-noise-gen)))
                    ))
            (phase (env (phases j))))
        (set! (tn hn) (* amp (sin phase)))
        (set! (un hn) (* amp (cos phase)))))
    (polyoid gen fm)))

(define (partial-amp-envs->gens envs amps dur)
  (if (pair? envs)
      (cons (make-env (car envs)
                      (car amps)
                      dur)
            (partial-amp-envs->gens (cdr envs) (cdr amps) dur))))

(define (partials->amp-scalars partials)
  (if (pair? partials)
      (cons (car (cdr partials))
            (partials->amp-scalars (cdr (cdr partials))))
      ()))

(definstrument (orch start-time dur frequency amplitude
                     partials
                     (amp-env #f)
                     (partial-amp-envs #f)
                     (vibrato-env '(0 0 1 0))
                     (gliss-env (float-vector 0 0 100 0))
                     (glissando-amount 0.0)
                     (reverb-amount 0.13)
                     (attack-noise-freq 1200.0)
                     (attack-noise-freq-env '(0 1 0.2 0 1.0 0))
                     (attack-noise-amp-env '(0 1 0.2 0 1.0 0))
                     (distance 3.0)
                     (degree 90.0)
                     (periodic-vibrato-rate 4.0) 
                     (random-vibrato-rate 16.0)
                     (periodic-vibrato-amplitude 0.0038)
                     (random-vibrato-amplitude 0.0026))
  (let* ((freq-scale (hz->radians frequency))
         (beg (seconds->samples start-time))
         (end (seconds->samples (+ start-time dur)))
         (ls (make-locsig :channels 2 :degree degree :distance distance :reverb reverb-amount))
         (amp-env (make-env amp-env amplitude dur))
         (attack-noise-freq-env (make-env attack-noise-freq-env 1.0 0.2))
         (attack-noise-amp-env (make-env attack-noise-amp-env 1.0 0.2))
         (attack-noise (make-rand-interp (hz->radians attack-noise-freq) 0.05)))
    (let* ((normalized-partials (vector->list (normalize-partials partials)))
           (num-partials (/ (length normalized-partials) 2)))
      (set! *orch-polyoid-params* (partials->polyoid-params normalized-partials))
      (set! *orch-amps* (partial-amp-envs->gens partial-amp-envs (partials->amp-scalars normalized-partials) dur))
      (set! *orch-phases* (partials->phases normalized-partials dur))
      (set! *orch-polyoid* (make-polyoid frequency *orch-polyoid-params*))
      (set! *orch-amp-env* amp-env)
      (set! *orch-vibrato-env* (make-env vibrato-env 1.0 dur))
      (set! *orch-vibrato-wave* (make-triangle-wave :frequency periodic-vibrato-rate :amplitude (* periodic-vibrato-amplitude freq-scale)))
      (set! *orch-random-vibrato-wave* (make-rand-interp random-vibrato-rate (* random-vibrato-amplitude freq-scale)))
      (set! *orch-gls-env* (make-env gliss-env (* glissando-amount freq-scale) dur)))
    (setf (locsig-ref ls 0) 1.0)
    (setf (locsig-ref ls 1) 1.0)
    (let ((vib 0.0))
      (do ((i beg (+ i 1))) 
          ((= i end))
        ;; apply vibrato envelope to both rate and amplitude 
        (set! vib (env *orch-vibrato-env*))
        (set! (mus-frequency *orch-vibrato-wave*)
          (+ periodic-vibrato-rate vib))
        (set! (mus-scaler *orch-vibrato-wave*)
          (+ (* periodic-vibrato-amplitude freq-scale)
             (* 0.0001 vib)))
        (locsig ls i (* (env *orch-amp-env*)
                        (polyoid-env^ *orch-polyoid*
                                      (+ (env *orch-gls-env*)
                                         (triangle-wave *orch-vibrato-wave*)
                                         (rand-interp *orch-random-vibrato-wave*))
                                      *orch-amps*
                                      *orch-phases*
                                      *orch-polyoid-params*
                                      attack-noise
                                      (env attack-noise-amp-env))))))))
                                      

;;; This function is modified from the Snd sources to allow
;;; configurable decay-length like the Common Lisp version has.
(definstrument (nrev* (reverb-factor 1.017) (lp-coeff 0.8) (volume 1.0) (decay-length 7.0))
  ;; reverb-factor controls the length of the decay -- it should not exceed (/ 1.0 .823)
  ;; lp-coeff controls the strength of the low pass filter inserted in the feedback loop
  ;; output-scale can be used to boost the reverb output
  (let ((dly-len (if (= (floor *clm-srate*) 44100)
		     #i(2467 2753 3217 3533 3877 4127 599 197 67 101 97 73 67 53 37)
		     (and (= (floor *clm-srate*) 22050)
			  #i(1237 1381 1607 1777 1949 2063 307 97 31 53 47 37 31 29 17))))
	(chan2 (> (channels *output*) 1))
	(chan4 (= (channels *output*) 4)))
	
    (if (not dly-len)
	(let ((srscale (/ *clm-srate* 25641))
	      (next-prime (lambda (val)
			    (do ((val val (+ val 2)))
				((or (= val 2)
				     (and (odd? val)
					  (do ((i 3 (+ i 2))
					       (lim (sqrt val)))
					      ((or (= 0 (modulo val i))
						   (> i lim))
					       (> i lim)))))
				 val)))))

	  (set! dly-len #i(1433 1601 1867 2053 2251 2399 347 113 37 59 53 43 37 29 19))
	  (do ((i 0 (+ i 1)))
	      ((= i 15))
	    (let ((val (floor (* srscale (dly-len i)))))
	      (if (even? val) (set! val (+ val 1)))
	      (set! (dly-len i) (next-prime val))))))

    (let ((len (+ (seconds->samples decay-length) (floor *clm-srate*) (framples *reverb*)))
	   (comb1 (make-comb (* .822 reverb-factor) (dly-len 0)))
	   (comb2 (make-comb (* .802 reverb-factor) (dly-len 1)))
	   (comb3 (make-comb (* .773 reverb-factor) (dly-len 2)))
	   (comb4 (make-comb (* .753 reverb-factor) (dly-len 3)))
	   (comb5 (make-comb (* .753 reverb-factor) (dly-len 4)))
	   (comb6 (make-comb (* .733 reverb-factor) (dly-len 5)))
	   (low (make-one-pole lp-coeff (- lp-coeff 1.0)))
	   (allpass1 (make-all-pass -0.700 0.700 (dly-len 6)))
	   (allpass2 (make-all-pass -0.700 0.700 (dly-len 7)))
	   (allpass3 (make-all-pass -0.700 0.700 (dly-len 8)))
	   (allpass4 (make-all-pass -0.700 0.700 (dly-len 9))) ; 10 for quad
	   (allpass5 (make-all-pass -0.700 0.700 (dly-len 11)))
	   (allpass6 (and chan2 (make-all-pass -0.700 0.700 (dly-len 12))))
	   (allpass7 (and chan4 (make-all-pass -0.700 0.700 (dly-len 13))))
	   (allpass8 (and chan4 (make-all-pass -0.700 0.700 (dly-len 14)))))

      (let ((filts (if (not chan2)
		       (vector allpass5)
		       (if (not chan4)
			   (vector allpass5 allpass6)
			   (vector allpass5 allpass6 allpass7 allpass8))))
	    (combs (make-comb-bank (vector comb1 comb2 comb3 comb4 comb5 comb6)))
	    (allpasses (make-all-pass-bank (vector allpass1 allpass2 allpass3))))
	
	(if chan4
	    (do ((i 0 (+ i 1)))
		((= i len))
	      (out-bank filts i
			(all-pass allpass4
				  (one-pole low
					    (all-pass-bank allpasses 
							   (comb-bank combs (* volume (ina i *reverb*))))))))
	    (if chan2
		(let ((gen1 (filts 0))
		      (gen2 (filts 1)))
		  (do ((i 0 (+ i 1)))
		      ((= i len))
		    (let ((val (all-pass allpass4
					 (one-pole low
						   (all-pass-bank allpasses 
								  (comb-bank combs (* volume (ina i *reverb*))))))))
		      (outa i (all-pass gen1 val))
		      (outb i (all-pass gen2 val)))))

		(let ((gen (filts 0)))
		  (do ((i 0 (+ i 1)))
		      ((= i len))
		    (outa i (all-pass gen
				      (all-pass allpass4
						(one-pole low
							  (all-pass-bank allpasses 
									 (comb-bank combs (* volume (ina i *reverb*))))))))))))))))

