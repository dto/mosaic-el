;; Basic config for Windows users of EmacsMosaic

;; enable melpa package support
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/"))
;;(package-initialize)

(package-refresh-contents)
(package-install 'use-package)
;;(package-install 'paren)
;;(package-install 'windmove)
(package-install 'jeison)
(package-install 'sr-speedbar)
(require 'use-package)

;; (use-package paren ; highlight matching parentheses
;;   :init (add-hook 'prog-mode-hook 'show-paren-mode)
;;   :config (setq show-paren-delay 0))

;; (use-package windmove ; focus windows with shift + arrow keys
;;   :bind (("S-<left>" . windmove-left)
;;          ("S-<right>" . windmove-right)
;;          ("S-<up>" . windmove-up)
;;          ("S-<down>" . windmove-down)))

(use-package sr-speedbar)

(require 'sr-speedbar)
(global-set-key (kbd "<f1>") 'sr-speedbar-toggle)
(setf sr-speedbar-right-side t)


(global-set-key (kbd "C-z") 'undo)
(global-set-key [(control tab)] 'other-window)

(winner-mode t)
(global-set-key [(control x) (left)] 'winner-undo)
(global-set-key [(control x) (right)] 'winner-redo)
;; (global-set-key (kbd "C-x C-b") 'ibuffer)

(autoload 'turn-on-eldoc-mode "eldoc" nil t)
(add-hook 'emacs-lisp-mode-hook 'turn-on-eldoc-mode)
(menu-bar-mode 1)
;;(desktop-save-mode 1)
;;(add-to-list 'desktop-globals-to-save 'file-name-history)
;;(add-hook 'auto-save-hook (lambda () (desktop-save-in-desktop-dir)))
(setq visible-bell t) 

(add-to-list 'load-path "~/mosaic/")
(byte-compile-file "~/cell-mode/cell-mode.el" t)
(add-to-list 'auto-mode-alist (cons "\\.cell" 'cell-mode))
(load "mosaic.el")
(load "mosaic-theme.el")
(cell-mode-insinuate)

(setf mosaic-suppress-snd-gui-p t)

(sr-speedbar-open)
;; (setq tabbar-background-color "gray20")
;; (setq tabbar-use-images nil)
(setq mosaic-use-ecasound-p nil)

(setq speedbar-show-unknown-files t)
(require 'org-mouse)
(setf org-startup-folded t)

;; fix ugly cygwin prompt
(when (mosaic-cygwin-p)
  (setenv "PS1" "\\u@\\h\\$ "))

(define-key-after
  (lookup-key global-map [menu-bar file])
  [save-some-buffers]
  '("Save some buffers" . save-some-buffers)
  'write-file)

