;;; soundmosaic.el --- routines for working with the original soundmosaic  -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2021  David O'Toole

;; Author: David O'Toole <dto@monad>
;; Keywords: multimedia

;; EmacsMosaic is Copyright (C) 2006-2007, 2019-2021 by David O'Toole <dto@xelf.me>
;; Author: David O'Toole <dto@xelf.me>
;; Keywords: audio, sound, music, vaporwave
;; License: MIT (full text follows)

;; Permission is hereby granted, free of charge, to any person obtaining a copy
;; of this software and associated documentation files (the "Software"), to deal
;; in the Software without restriction, including without limitation the rights
;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;; copies of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included in all
;; copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

;;; <working with the original soundmosaic>

(defvar* soundmosaic-id 0)

(defun* soundmosaic-output-file (&optional (id soundmosaic-id))
  (incf soundmosaic-id)
  (incf id)
  (cl-flet ((f () (format "soundmosaic-%d.wav" id)))
    (while (file-exists-p (f))
      (message "Skipping output file %d..." id)
      (incf id))
    (message "Choosing output file %d." id)
    (setf soundmosaic-id id)
    (f)))
  
(defvar* soundmosaic-program "soundmosaic")

(defvar* soundmosaic-partition-p t)

(defun* soundmosaic-options (&optional (time 0.1) (partition-p soundmosaic-partition-p))
  (append (list "-t" (format "%f" time))
	  (when partition-p (list "-p"))))

(defun* soundmosaic-arguments (&key time target output sources (partition-p soundmosaic-partition-p))
  (append (soundmosaic-options time partition-p)
	  (list target output)
	  sources))

(defun* soundmosaic-start-process (&key time target output sources partition-p)
  (apply #'start-process "*soundsoundmosaic*" "*soundsoundmosaic*"
	   soundmosaic-program
	   (soundmosaic-arguments :time time :target target :output output
			     :sources sources :partition-p partition-p)))

(defun* soundmosaic-start-process^ (&key time target output sources partition-p)
  (message (format "SOUNDMOSAIC: %S" (cons soundmosaic-program
			      (soundmosaic-arguments :time time :target target :output output
						:sources sources :partition-p partition-p)))))

(defvar* soundmosaic-sources-directory "~/Desktop/80s90s/")
(defvar* soundmosaic-targets-directory "~/Desktop/targets/")

(defun* soundmosaic-grab-sources (&optional (dir soundmosaic-sources-directory))
  (let ((d (file-name-as-directory dir)))
    (let ((files (directory-files d :full "\\(\\.flac\\|\\.wav\\)$")))
      (prog1 files
	(if files
	    (message "Searched %s, found %d sources in FLAC or WAV format." d (length files))
	  (message "Searched %s, found no sources!" d))))))

(defun* soundmosaic-grab-targets (&optional (dir soundmosaic-targets-directory))
  (let ((d (file-name-as-directory dir)))
    (let ((files (directory-files d :full "\\.wav$")))
      (prog1 files
	(if files
	    (message "Searched %s, found %d targets in WAV format." d (length files))
	  (message "Searched %s, found no targets!" d))))))

(defun* soundmosaic-choose-sources (&optional (n 3) (dir soundmosaic-sources-directory))
  (let ((sources (soundmosaic-grab-sources dir)))
    (when (>= (length sources) n)
      (subseq (derange sources) 0 (1- n)))))

(defun* soundmosaic-run* ()
  (dolist (target (soundmosaic-grab-targets))
    (soundmosaic-start-process :time 0.25 :target target
			   :output (soundmosaic-output-file)
			   :sources (soundmosaic-choose-sources)
			   :partition-p t)))
(provide 'soundmosaic)
;;; soundmosaic.el ends here


