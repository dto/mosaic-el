;; Scheme definitions for your session go in this file. These
;; definitions are loaded before each render job.
;;
;; Comments begin with semicolons and go to the end of the
;; line.
;;
;; Some examples of useful functions are below. You can edit them, or
;; replace them entirely.

(define my-pattern '(a a c d e f e h))

(define (my-processor)
  (chain (chop my-pattern) (screw vinyl)))

(define (my-simple-score-function)
  (weighted-distance :spectrum 1.0
                     :centroid 2.0
                     :tempo 0.1
                     :energy 1.0
                     :pitches 0.0))

(define (my-complex-score-function)
  (lambda (region-1 region-2)
    (+ (* 2.0 (spectrum-distance a b))
       (* 3.0 (centroid-distance a b))
       ;; square the energy distance instead of using a scalar
       (expt (total-energy-distance a b) 2))))

(define (my-query-1)
  (match-property-with-tolerance 'pitch 440.0 5.0))

(define (my-query-2 pitch)
  (match-property-with-tolerance 'pitch pitch 5.0))

(define (match-interval note interval)
  (match-and (match-note 'note note)
             (match-note 'note (+ note interval))))
