#+TITLE: Cell-mode 
#+AUTHOR: David O'Toole <dto@xelf.me>
#+OPTIONS: toc:3 *:nil
# #+INFOJS_OPT: path:http://orgmode.org/org-info.js
# #+INFOJS_OPT: view:info mouse:underline up:xelf.html home:http://xelf.me toc:t ftoc:t ltoc:t

** Overview

 Cell-mode implements an abstract spreadsheet control in an Emacs
 buffer.

** Screenshot and videos

 -  [[https://youtu.be/weCgCZCowv4][YouTube video 1]] 
 -  [[https://www.youtube.com/upload_thumbnail?v=sgVRMkTP9-E][YouTube video 2]]
 -  [[https://www.youtube.com/watch?v=E74iXyTLpyE][YouTube video 3]]

 file:cell2.png

** Features

 - Uses a simple file format based on Lisp property lists. Cell-mode
   files can be visited, saved, loaded, and so on just like any other
   text file. Works with Emacs' existing autosave and backup features.
 - You can cut, copy, and paste cells between sheets using typical
   Emacs shortcuts, as well as insert and delete rows and
   columns. 
 - Undo/redo support.
 - Execute sexps in cells, and other user defined actions.
 - Rectangle selection via keyboard and mouse-drag are available.
 - Object-orientation through the use of Emacs' included object
   system, EIEIO. Cells can contain any lisp value and respond
   polymorphically to events.
 - Contents can be collected and processed however you want.
 - Images can be displayed in cells.

** Purpose

The purpose of Cell-mode is to provide a major mode for abstract
spreadsheets that can be further customized by defining
application-specific Emacs Lisp minor modes that engage new cell and
sheet subclasses via EIEIO.

** Status

Cell-mode was originally written in 2006 and is currently being
revised. There are some bugs and it should be considered to be in an
alpha state. There is currently no topological sort (i.e. data
dependencies between cells) but this will be added.

** Issues 

 - Cursor does not display in image cells.
 - Needs more optimization. I recommend byte-compiling cell.el for best
   responsiveness.

** Getting started 

Download [[https://gitlab.com/dto/cell-mode][Cell-mode]] and place it in your load path. Then: 

 #+BEGIN_SRC
(require 'cell)
(add-to-list 'auto-mode-alist (cons "\\.cell" 'cell-mode))
 #+END_SRC

And try opening a new file, for example "/home/dto/test.cell". Or use
M-x cell-sheet-create.

Use "Control-H m" to get help on the keybindings.

To byte-compile and load cell-mode in one step, use: 

#+BEGIN_SRC
(byte-compile-file "/path/to/cell.el" t) 
#+END_SRC
