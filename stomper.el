;;; mosaic-stomper.el --- rhythm composer            -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2021  David O'Toole
;; Author: David O'Toole <dto@xelf.me> <deeteeoh1138@gmail.com>
;; Keywords: multimedia

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Stomper

(defvar mosaic-stomper-width 16
  "Width in columns of the stomper pattern.")

(defun mosaic-stomper-output-file ()
  (mosaic-session-file ".stomper.wav"))

(defclass mosaic-stomper (mosaic-chopper) ())

(cl-defmethod mosaic-stomper-samples ((stomper mosaic-stomper))
  (let ((cells (cell-collect-cells-by-row stomper)))
    (when (consp cells)
      (let ((vals (mapcar #'cl-first cells)))
        (unless (cl-some #'null vals)
          (cl-remove-if #'vectorp
                     (mapcar #'cell-find-value vals)))))))

(cl-defmethod mosaic-stomper-hits ((stomper mosaic-stomper) n)
  (let ((cells (cell-collect-cells-by-row-include-blanks stomper)))
    (when (consp cells)
      (let ((rows (mapcar #'cl-rest cells)))
        (let ((row (nth n rows)))
          (mapcar #'(lambda (c)
                      (if (or (null c)
                              (vectorp (cell-find-value c)))
                          nil
                        t))
                  row))))))

(cl-defmethod mosaic-stomper-data ((stomper mosaic-stomper))
  (let ((samples (mosaic-stomper-samples stomper)))
    (list :samples samples
          :width mosaic-stomper-width
          :dt mosaic-slice-size
          :hits (let (hits)
                  (dotimes (n (length samples))
                    (push (mosaic-stomper-hits stomper n)
                          hits))
                  (nreverse hits)))))

(cl-defmethod mosaic-stomper-scheme ((stomper mosaic-stomper))
  (apply #'mosaic-stomper-scheme* (mosaic-stomper-data stomper)))

(cl-defun mosaic-stomper-scheme* (&key samples
                                       (width mosaic-stomper-width)
                                       (dt mosaic-slice-size)
                                       (hits ()))
  (let (scheme)
    (cl-do ((ss samples (cdr ss))
         (n 0 (+ 1 n)))
        ((null ss) (reverse scheme))
      (let* ((hits* (nth n hits))
             (hits (cl-subseq hits* 0 (min width (length hits*))))
             (time 0.0))
        (dolist (hit hits)
          (when hit
            (push `(mosaic-stomp ,(mosaic-session-file (car ss)) ,time)
                  scheme))
          (cl-incf time dt))))))

(cl-defmethod mosaic-render* ((stomper mosaic-stomper))
  (mosaic-kill-thunks)
  (mosaic-clear-timestamps)
  (let ((sheet stomper))
    (with-current-buffer (slot-value sheet 'buffer)
      (cell-sheet-apply-settings sheet)
      (setf mosaic-engine-object (make-instance 'mosaic-engine))
      (mosaic-set-output-file (mosaic-stomper-output-file))
      (mosaic-tell-output-file)
      (mosaic-tell-session-directory mosaic-selected-session-directory)
      (mosaic-tell-tempo)
      ;; (mosaic-show-scheme)
      (let ((scheme (mosaic-stomper-scheme stomper)))
        ;; close existing sound if it is already open
        (mosaic-tell-scheme `(let ((s (find-sound ,(mosaic-stomper-output-file))))
                               (when (soundp s)
                                 (close-sound s))))
        ;; send the mix list and pad to length
        (mosaic-tell-scheme-file `(begin
                                   (mosaic-pad
                                    (find-sound
                                     (with-sound (:output ,(mosaic-stomper-output-file)
                                                          :channels 2
                                                          :header-type mus-riff
                                                          :sample-type mus-lshort
                                                          :scaled-to 0.9)
                                                 ,@scheme))
                                    ,(* mosaic-slice-size mosaic-stomper-width))
                                   (write-timestamp-file)))
        (mosaic-wait-for-file (mosaic-timestamp-file)
          (mosaic-clear-timestamps)
          (mosaic-set-status-icon :ok)
          (mosaic-update-header-line-in-all-sheets)
          (mosaic-show-scheme-dwim))))))

(defun mosaic-toggle-cell-x ()
  (interactive)
  (cell-with-current-cell-sheet
   (cell-push-undo-history)
   (let ((c (cell-grid-get grid cursor-row cursor-column)))
     (if c
         (cell-grid-set grid cursor-row cursor-column nil)
       (cell-grid-set grid cursor-row cursor-column (cell-sexp->cell "x"))))
   (cell-sheet-update)))

(define-key cell-mode-map (kbd "`") 'mosaic-toggle-cell-x)

(provide 'mosaic-stomper)
;;; mosaic-stomper.el ends here
