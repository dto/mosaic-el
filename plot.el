;;; mosaic-plot.el --- showing waveforms within emacs using gnuplot  -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2021  David O'Toole
;; Author: David O'Toole <dto@xelf.me> <deeteeoh1138@gmail.com>
;; Keywords: multimedia

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Making waveform images with Gnuplot

(defun mosaic-waveform-image-file (sound-file)
  "Return the name of the waveform image corresponding to SOUND-FILE."
  (concat "." sound-file ".waveform-image.png"))

(defun mosaic-waveform-template-file (sound-file)
  "Return the name of the Gnuplot script corresponding to SOUND-FILE."
  (concat "." sound-file ".waveform-template.gpi"))

(defvar mosaic-waveform-template nil)

(setf mosaic-waveform-template
      "set term png size %d,%d
set output \"%s\"
set yr [-32767:32768]
set grid x
set rmargin 0
set lmargin 0             
set tmargin 0
set bmargin 0
unset key
unset tics
unset border
set obj 1 rectangle behind from screen 0,0 to screen 1,1
set obj 1 fillstyle solid 1.0 fillcolor rgbcolor '%s'
%s
plot '< cat -' binary filetype=bin format='%%int16' endian=little array=1:0 every %d with lines lt rgb '%s'")

(defcustom mosaic-waveform-foreground-color "#bebebe"
  "Foreground color of waveform data."
  :type 'color
  :tag "Waveform foreground color"
  :group 'mosaic)

(defcustom mosaic-waveform-background-color "#f5f5f5"
  "Background color of waveform data."
  :type 'color
  :tag "Waveform background color"
  :group 'mosaic)

(defcustom mosaic-waveform-offset-color "#ff0000"
  "Color of waveform offset lines."
  :type 'color
  :tag "Waveform offset line color"
  :group 'mosaic)

(defcustom mosaic-waveform-label-color "#0000ff"
  "Color of waveform labels."
  :type 'color
  :tag "Waveform label color"
  :group 'mosaic)

(defvar mosaic-plot-default-data-width 64)

(defun mosaic-waveform-offset-plot-lines (offsets labels data-width data-length)
  (if (null offsets)
      ""
    (let (lines (n 0))
      (push "set style arrow 2 nohead\n" lines)
      (push "set arrow arrowstyle 2\n" lines)
      (while offsets
        (let ((x (/ (float (car offsets)) data-length)))
          (push (format
                 "set arrow from graph %f, graph 0 to graph %f , graph 1 lw 1 lc rgb '%s' front\n
set label front '%d' at graph %f, graph 0.15 tc '%s' font ',10' \n"
                 x x mosaic-waveform-offset-color n (+ x 0.005) mosaic-waveform-label-color)
                lines)
          (when labels
            (push (format
                   "set label front '%s' at graph %f, graph 0.3 tc 'black' font ',10' \n"
                   (or (nth n labels) ".")
                   (+ x 0.005) mosaic-waveform-label-color)
                  lines)))
        (cl-incf n)
        (pop offsets))
      ;;(push "show arrow\n" lines)
      (apply #'concat (reverse lines)))))

(defcustom mosaic-waveform-default-width 200
  "Default width in pixels of a waveform."
  :type 'integer
  :tag "Waveform default width (pixels)"
  :group 'mosaic)

(defcustom mosaic-waveform-default-height 70
  "Default height in pixels of a waveform."
  :type 'integer
  :tag "Waveform default height (pixels)"
  :group 'mosaic)

(defcustom mosaic-waveform-zoomed-width 600
  "Zoomed width in pixels of a waveform."
  :type 'integer
  :tag "Waveform zoomed width (pixels)"
  :group 'mosaic)

(defcustom mosaic-waveform-zoomed-height 100
  "Zoomed height in pixels of a waveform."
  :type 'integer
  :tag "Waveform zoomed height (pixels)"
  :group 'mosaic)

(cl-defun mosaic-render-waveform (&key input-file output-file
                                     (width mosaic-waveform-default-width)
                                     (height mosaic-waveform-default-height)
                                     (background-color mosaic-waveform-background-color)
                                     (foreground-color mosaic-waveform-foreground-color)
                                     offsets (data-width mosaic-plot-default-data-width)
                                     (labels ())
                                     (data-length nil))
  "Use Gnuplot to render a PNG waveform."
  (let ((template (format mosaic-waveform-template
                          width
                          height
                          (mosaic-session-file output-file)
                          background-color
                          (mosaic-waveform-offset-plot-lines offsets labels data-width data-length)
                          data-width
                          foreground-color))
        (template-file (mosaic-session-file
                        (mosaic-waveform-template-file input-file))))
    (with-temp-buffer
      (insert template)
      (mosaic-write-file template-file))
    (shell-command (format "sox %s -t s16 --channels 1 - | gnuplot %s"
                           (mosaic-session-file input-file)
                           template-file))))

(defun mosaic-find-sound-length (file)
  (interactive "fFind length of sound file: ")
  (mosaic-tell-scheme
   `(begin (let ((sound (open-sound (session-file ,file))))
             (mosaic-tell-emacs (framples sound))
             (close-sound sound)))))

(defun mosaic-find-waveform (sound-file &optional force)
  (let ((waveform (mosaic-session-file (mosaic-waveform-image-file sound-file))))
    (if (and (not force) (file-exists-p waveform))
        waveform
      (prog1 waveform
        (mosaic-render-waveform :input-file sound-file
                                :output-file (file-name-nondirectory waveform))))))

(defun mosaic-insert-waveform (sound-file &optional force)
  (insert-image (create-image (mosaic-find-waveform sound-file force))))

(defmacro mosaic-wait-for-waveform (sound-file &rest body)
  (let ((waveform (gensym)))
    `(let ((,waveform (mosaic-find-waveform ,sound-file)))
       (mosaic-wait-for-file ,waveform
         ,@body))))

(cl-defun mosaic-find-all-waveforms (&key force files)
  (interactive)
  (dolist (file (mapcar #'file-name-nondirectory
                        (or files (mosaic-find-sound-files (mosaic-session-directory)))))
    (mosaic-find-waveform file force)))

(defun mosaic-clear-image-cache (&optional filter)
  (clear-image-cache filter))

(provide 'mosaic-plot)
;;; mosaic-plot.el ends here
