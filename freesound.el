;;; mosaic-freesound.el --- Freesound support        -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2021  David O'Toole
;; Author: David O'Toole <dto@xelf.me> <deeteeoh1138@gmail.com>
;; Keywords: multimedia

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

;;; Freesound.org support (experimental)

(require 'jeison)

(defvar mosaic-freesound-client-id "vJVtYvNVFayerAbLXpKs")
(defvar mosaic-freesound-api-key "oTsUz2JisOPXdLLQB4ChJ4JxxvzUYdCYZhcQzAlb")
(defvar mosaic-freesound-permission-granted-url
  "https://freesound.org/home/app_permissions/permission_granted/")
(defvar mosaic-freesound-oauth-client-id nil)
(defvar mosaic-freesound-oauth-client-secret nil)
(defvar mosaic-curl-program "curl")
(defvar mosaic-freesound-oauth-user-key nil)
(defvar mosaic-freesound-oauth-user-key-file (expand-file-name "~/.mosaic-user-key.txt"))
(defun mosaic-read-key () (ignore-errors (load (mosaic-project-file "mosaic-key.elc"))))
(defun mosaic-curl-output-file ()
  (mosaic-project-file ".mosaic-curl.out"))
(defun mosaic-sound-metadata-file (file)
  (mosaic-session-file (concat (file-name-nondirectory file)
                               ".eieio")))

(defun mosaic-delete-curl-output-file ()
  (ignore-errors (delete-file (mosaic-curl-output-file))))

(cl-defun mosaic-curl (request &optional (output-file (mosaic-curl-output-file)))
  (message "Mosaic: Sending Freesound request: %s" request)
  (mosaic-delete-curl-output-file)
  (if (zerop (call-process mosaic-curl-program nil nil nil
                           "-H" (concat "Authorization: Token " mosaic-freesound-api-key)
                           (url-encode-url request)
                           "--output" output-file))
      (message "Mosaic: Sending Freesound request... Done.")
    (mosaic-error "Mosaic: Freesound request failed.")))

(cl-defun mosaic-curl-output ()
  (with-temp-buffer
    (insert-file-contents-literally (mosaic-curl-output-file))
    (buffer-substring-no-properties (point-min) (point-max))))

(defvar mosaic-sounds ())

(jeison-defclass mosaic-sound-data^ ()
  ((id) (url) (name) (tags) (description) (geotag) (created) (license) (type)
   (channels) (filesize) (bitrate) (bitdepth) (duration) (samplerate) (username)
   (pack) (download) (bookmark) (previews) (images) (num_downloads) (avg_rating)
   (num_ratings) (rate) (comments) (num_comments) (comment) (similar_sounds)
   (analysis) (analysis_stats) (analysis_frames) (ac_analysis)))

(defclass mosaic-sound (eieio-persistent eieio-speedbar-file-button)
  ((local-file :initform nil :initarg :local-file :accessor mosaic-sound-local-file)
   (data :initform nil :initarg :data :accessor mosaic-sound-data)))

(cl-defmethod eieio-speedbar-object-buttonname ((s mosaic-sound))
  (or (slot-value s 'local-file)
      "No local file."))

(cl-defmethod mosaic-sound-register ((s mosaic-sound))
  (cl-pushnew s mosaic-sounds :test 'eq))

(cl-defmethod mosaic-sound-unregister ((s mosaic-sound))
  (setf mosaic-sounds (delete s mosaic-sounds)))

(defun mosaic-file->sound (file)
  ;; TODO: fill in more fields here
  (let ((sound (make-instance 'mosaic-sound
                              :local-file file)))
    (setf (mosaic-sound-local-file sound)
          (file-name-nondirectory file))
    sound))

(defun mosaic-find-session-sounds ()
  (mapcar #'mosaic-file->sound (mosaic-find-sound-files (mosaic-session-directory))))

(cl-defmethod mosaic-sound-preview-url ((s mosaic-sound))
  (cdr (assoc 'preview-hq-ogg (slot-value (mosaic-sound-data s) 'previews))))

(cl-defmethod mosaic-sound-preview-local-file ((s mosaic-sound))
  (concat (file-name-sans-extension (slot-value (mosaic-sound-data s) 'name))
          ".ogg"))

(cl-defmethod mosaic-sound-download-preview ((s mosaic-sound))
  (let ((local-file (mosaic-sound-preview-local-file s)))
    (mosaic-curl (mosaic-sound-preview-url s) local-file)
    (setf (mosaic-sound-local-file s) local-file)))

(cl-defmethod mosaic-sound-import-preview ((s mosaic-sound))
  (let ((preview-file (mosaic-sound-preview-local-file s)))
    (copy-file preview-file
               (mosaic-session-file (file-name-nondirectory preview-file)))))

(cl-defmethod mosaic-sound-import-preview :after ((s mosaic-sound))
  (mosaic-sound-register s))

(jeison-defclass mosaic-sound-list^ nil
  ((cl-count) (next)
   (results :initarg :results :type (list-of mosaic-sound-data^))
   (previous)))

(defvar mosaic-freesound-current-page-number 0)

(cl-defun mosaic-freesound-search-text (query)
  (mosaic-curl (concat "https://freesound.org/apiv2/search/text/?" query)))

(cl-defun mosaic-freesound-read-sound-list ()
  (jeison-read mosaic-sound-list^ (mosaic-curl-output)))

(cl-defun mosaic-freesound-find-sound (sound-id)
  (mosaic-curl (format "https://freesound.org/apiv2/sounds/%d/" sound-id)))

(cl-defun mosaic-freesound-read-sound^ ()
  (jeison-read mosaic-sound-data^ (mosaic-curl-output)))

(cl-defun mosaic-freesound-read-sound ()
  (make-instance 'mosaic-sound :data (mosaic-freesound-read-sound^)))

(defclass mosaic-browser (mosaic-chopper eieio-speedbar-directory-button)
  ((sound-list :initform nil :initarg :sound-list :accessor mosaic-browser-sound-list)
   (query :initform nil :initarg :query :accessor mosaic-browser-query)
   (current-page :initform 1 :initarg :current-page :accessor mosaic-browser-current-page)
   (rows :initform 50)
   (cols :initform 24)))

(cl-defmethod mosaic-browser-sounds ((b mosaic-browser))
  (when (mosaic-browser-sound-list b)
    (slot-value (mosaic-browser-sound-list b) 'results)))

(cl-defmethod initialize-instance :after ((b mosaic-browser) &rest slots)
  (when (mosaic-browser-sound-list b)
    (cell-with-current-cell-sheet
     (cell-sheet-move-bob)
     (cell-grid-set grid 0 0 (cell-sexp->cell "[cell-sheet-class mosaic-browser]"))
     (cell-sheet-move-cursor-down)
     (let (rows)
       (dolist (sound (mosaic-browser-sounds b))
         (push (list (file-name-sans-extension (slot-value sound 'name))
                     (slot-value sound 'duration))
               rows))
       (cell-copy-cells-from-sexps (reverse rows))
       (cell-sheet-paste))
     (cell-sheet-update))))

(defun mosaic-browser-buffer ()
  (save-current-buffer
    (mosaic-visit-browser)
    (current-buffer)))

(defun mosaic-browser-sheet () 
  (save-current-buffer
    (save-window-excursion
      (mosaic-visit-browser)
      cell-current-sheet)))

(defun mosaic-search-browser (terms)
  (interactive "sSearch terms: ")
  (mosaic-freesound-search-text terms)
  (let ((sound-list (mosaic-freesound-read-sound-list)))
    (switch-to-buffer (mosaic-browser-buffer))
    (setf cell-current-sheet
          (make-instance 'mosaic-browser
                         :current-page 1
                         :query terms
                         :sound-list sound-list))))

(cl-defmethod eieio-speedbar-object-buttonname ((s cell-sheet))
  (file-name-nondirectory (buffer-file-name (cell-sheet-buffer s))))

(cl-defmethod eieio-speedbar-handle-click ((s cell-sheet))
  (find-file (buffer-file-name (cell-sheet-buffer s))))

(cl-defmethod eieio-speedbar-object-children ((b mosaic-browser))
  (mosaic-browser-sounds b))

;; (mosaic-freesound-search-text "727")
;; (mosaic-freesound-read-sound-list)
;; (mosaic-freesound-find-sound 1234)
;; (mosaic-freesound-read-sound)
;; (setf *foo* (mosaic-freesound-read-sound))
;; (mosaic-sound-preview-url *foo*)
;; (mosaic-sound-download-preview *foo*)
;; (mosaic-sound-import-preview *foo*)
;; (mosaic-search-browser "snare")

(provide 'mosaic-freesound)
;;; mosaic-freesound.el ends here
