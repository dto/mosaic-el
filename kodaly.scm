;; 9/3/2019

;; The first ten seconds of Zoltan Kodaly's Sonata for solo cello are
;; analyzed at various time slice sizes to produce the MIDI note
;; number sequences below. Pitch is detected by simple
;; autocorrelation, this needs to be improved. 

;; The note sequences are then analyzed by my implementation of the
;; Sequitur algorithm, and I present the results and attempt to
;; analyze them.

;; The included file kodaly-stretched.wav is processed with
;; Paulstretch to make it more amenable to the pitch detector. If you
;; want to hear the actual music, check it out on Youtube:
;; https://www.youtube.com/watch?v=phygv_Et9sQ

;;;(load "/home/dto/Desktop/mosaic/mosaic.scm")

;; Here are the MIDI note numbers from the opening bars of the sonata,
;; taken at the largest timeslice.

(define kodaly-4 '(48 41 54 52 38 51 47 47 47 47 38 38 55 41 47 47 52
47 47 47 47 47 47 54 47 49 61 61 70 61 89 70 65 65 50 57 91 57 55 55
54 54 47 47 57 47 47 47 47 47 54 47 51 61 62 70 61 89 50 50 61 64 67
87 75 57 58 58 58 58 45 54 53 47 47 87 57 47 57 61 48 49 61 60 72 73
93 74 54 54 52 52 75 76 52 67 73 67 47 42 61 54 54 74 47 61 61 60 52
74 66 76 88 88 71 72))

;; Obviously note 47 (approx. 123.47 Hz) comes up a lot. I think 47 is
;; the second note in the song, based on the way it recurs and on how
;; it sounds when I generate 123.47 Hz in Audacity. Because there are
;; transient polyphonies in this passage, and because the pitch
;; detection is currently quite simple, I'm expecting some of the
;; numbers to be wrong---but repeated stretches of the same pitch
;; *should* reveal the existence of an empirical note, and repetitions
;; of these should be discernible in the output from Sequitur.

;; Let's do it!

;; (find-structure kodaly-4 :show? #t)
;; (format #t "~W" (*symbols* 'AXIOM^))

;; -results-4----------------------------------------------------------

;; ---axiom:-----------------------------------------------------------
;; 48 41 i 38 51 b* 38 38 55 41 a* 52 b* a* c h 61 e 70 65 65 50 57 91 57 55
;; 55 54 c* g* b 47* c 51 61 62 e 50 50 61 64 67 87 75 57 f f 45 54 53 a* 87
;; 57 g 61 48 h 60 72 73 93 74 54 i 52 75 76 52 67 73 67 47* 42 61 54 54
;; 74 47* 61 61 60 52 74 66 76 88 88 71 72

;; ---rules:-----------------------------------------------------------
;; g -> (47 57)       *
;; f -> (58 58)
;; c -> (54 47)       *
;; h -> (49 61)
;; a -> (47 47)       **
;; i -> (54 52)
;; e -> (70 61 89)
;; b -> (a a)

;; ---analysis:--------------------------------------------------------
;; Note 47 appears three times in the axiom and four times in the
;; rules. These are marked with asterisks above. Places where a
;; variable containing a 47 appears in the axiom are also marked.

;; Notice how variable A, composed of (47 47) and given parens where
;; it appears in the axiom to emphasize it, appears twice early on as
;; a long note and then once again later as a long note--much like the
;; melody, I think. 

;; Then where C G B appears just later, there is a hidden "47 47"
;; Because C-G becomes 54 47 - 47 57. And several other G and C
;; appear.

;; The ending appears to rise upward to a high of 88, with the
;; following 71 and 72 possibly being reverb tail of prev notes
;; causing poor pitch detection in the last two slices.

;; There is one long stretch of four note 58's in the middle, there
;; are no other 58's anywhere.

;; 9/4/2019

(define kodaly-16* '(41 66 52 51 59 48 53 50 58 59 60 59 60 61 73 62
62 69 47 49 53 59 59 48 59 60 73 73 62 60 81 58 69 69 52 56 60 58 54
59 72 73 64 66 64 58 61 65 54 52 65 61 61 77 65 83 83 71))

;; (let ((s (find-structure kodaly-32*)))
;;   (show-structure s)
;;   (format #t "~W" (s +axiom-symbol+)))

;; axiom -> 67 41 59 67 58 a a 60 51 47 57 54 53 50 60 b 59 i b d 73
;; f 63 c c h 52 50 50 53 53 d 60 59 49 d 59 60 f 73 61 c 61 62
;; 81 62 60 h 69 49 52 43 55 i 35 b 54 d f 83 64 64 j 65 64 79 b
;; 62 78 65 60 54 55 52 j 59 61 74 62 73 77 60 65 84 k k 72
;; a -> (52 49)
;; b -> (58 60)
;; c -> (62 62)
;; d -> (60 60)
;; f -> (61 73 73)
;; h -> (69 69 69)
;; i -> (58 61)
;; j -> (66 66)
;; k -> (83 83)

(define kodaly-32* '(67 41 59 67 58 52 49 52 49 60 51 47 57 54 53 50
60 58 60 59 58 61 58 60 60 60 73 61 73 73 63 62 62 62 62 69 69 69 52
50 50 53 53 60 60 60 59 49 60 60 59 60 61 73 73 73 61 62 62 61 62 81
62 60 69 69 69 69 49 52 43 55 58 61 35 58 60 54 60 60 61 73 73 83 64
64 66 66 65 64 79 58 60 62 78 65 60 54 55 52 66 66 59 61 74 62 73 77
60 65 84 83 83 83 83 72))

;; (let ((s (find-structure kodaly-32*)))
;;   (show-structure s)
;;   (format #t "~W" (s +axiom-symbol+)))

;; AXIOM -> (67 41 59 67 58 A A 60 51 47 57 54 53 50 60 B 59 I B D 73 F 63
;; C C H 52 50 50 53 53 D 60 59 49 D 59 60 F 73 61 C 61 62 81 62 60 H 69
;; 49 52 43 55 I 35 B 54 D F 83 64 64 J 65 64 79 B 62 78 65 60 54 55 52 J
;; 59 61 74 62 73 77 60 65 84 K K 72)
;; A -> (52 49)
;; B -> (58 60)
;; C -> (62 62)
;; D -> (60 60)
;; F -> (61 73 73)
;; H -> (69 69 69)
;; I -> (58 61)
;; J -> (66 66)
;; K -> (83 83)


(define kodaly-64* '(46 53 43 41 55 62 66 57 57 61 51 50 51 49 50 48 55
49 54 57 60 53 55 41 53 50 56 46 43 44 57 52 48 42 56 59 36 59 61 61
59 58 60 57 57 59 59 60 59 59 59 59 61 61 73 60 73 73 73 61 60 73 61
60 62 62 74 62 62 62 63 69 69 68 69 69 47 69 61 66 48 53 42 52 54 62
47 51 55 43 59 53 61 56 55 46 59 57 58 59 60 59 58 59 59 54 62 73 62
74 73 73 73 73 50 62 62 63 62 61 62 62 81 74 61 62 63 69 69 68 69 70
69 69 58 57 69 54 39 55 53 55 49 55 59 51 54 59 60 50 48 48 59 58 58
60 61 73 59 57 59 60 73 73 73 62 61 61 64 64 65 65 97 66 66 67 65 67
64 64 64 64 65 61 59 59 59 60 66 65 66 67 58 60 60 52 55 55 56 53 53
67 57 51 60 59 60 59 58 73 71 64 73 62 73 59 64 66 66 67 67 69 83 83
83 83 83 83 71 71 83 71 72))

(let ((s (find-structure kodaly-64*)))
  (show-structure s)
  (format #t "~W" (s +axiom-symbol+)))

;; (define kodaly-6 '(48 54 41 54 54 51 38 38 52 47 38 43 47 48
;; 47 38 37 37 55 42 51 47 52 47 52 47 47 47 47 52 47 47 55 47 47 47 47
;; 58 62 61 61 61 70 60 62 89 65 66 65 50 89 50 53 57 91 57 57 55 55 57
;; 54 54 57 47 47 47 57 57 47 47 47 47 47 52 53 54 57 47 51 88 71 62 61
;; 61 61 61 61 50 61 50 61 64 70 67 61 87 75 89 58 58 57 58 58 44 43 45
;; 39 54 53 47 47 47 65 88 57 47 61 57 61 61 48 62 70 61 64 60 72 52 52
;; 93 66 68 54 54 52 52 52 53 75 53 67 52 52 54 73 72 69 47 42 42 61 66
;; 42 54 61 62 47 61 61 61 61 73 52 67 67 66 57 88 88 71 71 71 71 75))

;; (find-structure kodaly-6 :show? #t)
;; (format #t "~W" (*symbols* 'AXIOM^))

;; -results-6----------------------------------------------------------
;; ---axiom:-----------------------------------------------------------
;; 48 54 41 e 51 38 38 52 a* 43 47* 48 a* 37 37 55 42 51 b* b* d 52 c* 55 d 58
;; j 70 60 62 89 65 66 65 50 89 50 53 57 91 f 55 55 57 e 57 g* f g* c* p 54
;; m* 51 r j 61 k k n 70 67 61 87 75 89 l 57 l 44 43 45 39 54 53 g* 65 88 m*
;; 61 57 q 48 62 70 n 60 72 o 93 66 68 e o p 75 53 67 o 54 73 72 69 47* 42
;; 42 61 66 42 54 61 62 47* q q 73 52 67 67 66 57 88 r 71 71 71 75
;; ---rules:-----------------------------------------------------------
;; g -> (c 47)       *
;; f -> (57 57)      
;; a -> (47 38)      *
;; c -> (47 47)      **
;; j -> (62 q 61)    
;; o -> (52 52)
;; r -> (88 71)
;; p -> (52 53)
;; l -> (58 58)
;; m -> (57 47)      *
;; k -> (61 50)
;; e -> (54 54)
;; q -> (61 61)
;; b -> (47 52)      *
;; n -> (61 64)
;; d -> (c c)
;; --------------------------------------------------------------------

;; (define kodaly-8 '(48 49 41 54 54 42 52 54 38 38 51 51 47 55
;; 47 38 47 47 47 44 38 38 38 38 55 41 41 51 47 47 47 47 52 47 47 47 47
;; 47 47 47 47 47 47 57 47 47 54 47 47 47 49 89 61 65 61 70 70 70 61 61
;; 89 65 70 50 65 65 65 50 50 51 57 91 91 57 57 57 55 55 55 54 54 47 54
;; 47 47 47 47 57 57 57 47 47 47 47 47 47 47 52 47 47 54 47 47 89 51 91
;; 61 61 62 62 70 71 61 70 89 85 50 50 50 65 61 62 64 68 67 86 87 86 75
;; 88 57 57 58 58 58 58 58 58 58 58 45 54 54 74 53 47 47 47 47 71 87 90
;; 57 70 47 58 57 61 61 61 48 48 49 49 61 61 60 61 72 52 73 52 93 90 74
;; 66 54 54 54 52 52 52 52 53 75 52 76 47 52 69 67 54 73 75 67 47 47 54
;; 42 65 61 42 54 54 54 52 74 47 47 62 61 60 61 60 60 64 52 54 74 66 66
;; 57 76 89 88 90 88 71 71 71 72 72))

;; (find-structure kodaly-8 :show? #t)
;; (format #t "~W" (*symbols* 'AXIOM^))

;; -results-8----------------------------------------------------------
;; ---axiom:-----------------------------------------------------------
;; p 41 e 42 v a 51 b 55 47 38 d 44 a a 55 41 41 b d 52 i d
;; g c f 49 89 61 65 k 70 70 j 89 65 70 m 65 65 l 51 57 91 91 h
;; 57 55 55 55 e 47 f g h i r s c 89 51 91 j 62 62 70 71 k 89
;; 85 l m 61 62 64 68 67 86 87 86 75 88 h o o 45 e 74 53 d 47 71
;; 87 90 57 70 47 58 57 j 61 48 p 49 j u 72 52 73 52 93 90 w t q
;; q 53 75 52 76 r 69 67 54 73 75 67 s 42 65 61 42 t 52 74 c 62 61
;; u 60 60 64 v w 66 57 76 89 88 90 88 71 71 71 72 72
;; ---rules:-----------------------------------------------------------
;; g -> (47 57)
;; t -> (e 54)
;; f -> (54 d)
;; v -> (52 54)
;; a -> (38 38)
;; h -> (57 57)
;; j -> (61 61)
;; o -> (n n)
;; r -> (47 52)
;; p -> (48 49)
;; s -> (c 54)
;; l -> (50 50)
;; m -> (50 65)
;; k -> (61 70)
;; i -> (d d)
;; e -> (54 54)
;; u -> (60 61)
;; q -> (52 52)
;; w -> (74 66)
;; b -> (51 47)
;; n -> (58 58)
;; d -> (47 47 47)
;; --------------------------------------------------------------------
